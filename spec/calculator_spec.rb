require 'calculator'

RSpec.describe Calculator do

  before do
    @calculator = Calculator.new
  end
  after do
    puts = "YESSSS"
  end

  context "#topla" do
    it "returns sum of number 1 and 2" do
      expect(@calculator.topla(3, 5)).to eq 8
    end

    it "raises not a number error" do
      expect { @calculator.topla("serhat", 50) }.to raise_error(NotANumberError, "Lütfen bir sayı giriniz")
    end

  end

  context "#cikar" do
    it "returns sum of number 1 and 2" do
      expect(@calculator.cikar(3, 5)).to eq -2
    end
    it "raises not a number error" do
      expect { @calculator.cikar("serhat", 50) }.to raise_error(NotANumberError, "Lütfen bir sayı giriniz")
    end

    context "#multiple" do
      it "returns sum of number 1 and 2" do
        expect(@calculator.multiple(3, 5)).to eq 15
      end
      it "raises not a number error" do
        expect { @calculator.multiple("serhat", 50) }.to raise_error(NotANumberError, "Lütfen bir sayı giriniz")
      end
      context "#bolme" do
        it "returns sum of number 1 and 2" do
          expect(@calculator.bolme(10, 5)).to eq 2
        end
        it "raises not a number error" do
          expect { @calculator.bolme("serhat", 50) }.to raise_error(NotANumberError, "Lütfen bir sayı giriniz")
        end
        it "zero division error" do
          expect { @calculator.bolme(5, 0) }.to raise_error(ZeroDivisionError, "bir sayı sıfıra bölünemez")
        end
  end
  end
end

end
