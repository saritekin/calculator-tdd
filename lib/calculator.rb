class NotANumberError < StandardError
end

class Calculator
  HataMesajı = "Lütfen bir sayı giriniz"

  def topla(sayi1, sayi2)
    numbers?(sayi1, sayi2)
    sayi1 + sayi2
  end

  def cikar(sayi1, sayi2)
    numbers?(sayi1, sayi2)
    sayi1 - sayi2
  end

  def multiple(sayi1, sayi2)
    numbers?(sayi1, sayi2)
    sayi1 * sayi2
  end

  def bolme(sayi1, sayi2)
    numbers?(sayi1, sayi2)
    fail ZeroDivisionError, "bir sayı sıfıra bölünemez" if sayi2 == 0
    sayi1 / sayi2

  end

  private

  def numbers?(*numbers)
    fail NotANumberError, HataMesajı unless [*numbers].all? {|sayi| sayi.is_a?(Fixnum)}
  end

end